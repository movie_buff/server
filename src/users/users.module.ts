import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersResolver } from './graphql/users.resolver';
import { UserSubscriber } from './graphql/users.subscriber';
import { User } from './user.entity';
import { UsersService } from './users.service';

@Module({
  imports: [TypeOrmModule.forFeature([User])],
  providers: [UsersService, UsersResolver, UserSubscriber],
  exports: [UsersService],
})
export class UsersModule {}
