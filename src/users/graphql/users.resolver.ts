import { NotFoundException } from '@nestjs/common';
import { Args, Query, Resolver } from '@nestjs/graphql';
import { User } from '../user.entity';
import { UsersService } from '../users.service';

@Resolver()
export class UsersResolver {
  constructor(private readonly usersService: UsersService) {}

  @Query(() => User)
  async getUser(@Args('email') email: string): Promise<User> {
    const data = this.usersService.findOneBy(email);
    if (typeof data === 'string') {
      throw new NotFoundException(email);
    }

    return data;
  }

  // @Mutation(() => Boolean)
  // async createUser(@Args('signUpInput') signUpInput: SignUpInput): boolean {
  //   const data = this.usersService.addUser(signUpInput);
  //   if (typeof data === 'string') {
  //     throw new NotFoundException(signUpInput);
  //   }
  //   return data;
  // }
}
