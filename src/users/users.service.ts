import { Injectable } from '@nestjs/common';
import { handleError } from 'src/utils/utils';
import { DataSource } from 'typeorm';
import { User } from './user.entity';

@Injectable()
export class UsersService {
  constructor(private dataSource: DataSource) {}

  async findOneBy(email: string): Promise<User> {
    try {
      const user = this.dataSource.transaction(
        async (manager): Promise<User> => {
          return await manager.findOneBy(User, { email });
        },
      );

      return user;
    } catch (error) {
      handleError(error);
    }
  }
  // findAll(): Promise<User[]> {
  //   return this.find();
  // }

  // findOne(id: number): Promise<User> {
  //   return this.findOneBy({ id });
  // }

  // async remove(id: string): Promise<void> {
  //   await this.delete(id);
  // }
}
