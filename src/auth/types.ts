export type JwtPayload = {
  userId: string;
  email: string;
};

export type JwtPayloadWithRefreshToken = JwtPayload & { refreshToken: string };

export type TokenData = {
  accessToken: string;
  refreshToken: string;
};
