import { ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { GqlExecutionContext } from '@nestjs/graphql';
import { AuthGuard } from '@nestjs/passport';
import { Observable } from 'rxjs';
import { ROLES_KEY } from './decorators/roles.decorator';
import { Role } from './roles/role.enum';

@Injectable()
export class GqlAuthGuard extends AuthGuard('jwt') {
  constructor(private reflector: Reflector) {
    super();
  }
  getRequest(context: ExecutionContext) {
    const ctx = GqlExecutionContext.create(context);
    return ctx.getContext().req;
  }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const requiredRoles = this.reflector.getAllAndOverride<Role[]>(ROLES_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);

    // If no roles then it is a public route
    if (!requiredRoles) {
      return true;
    }

    console.log('Required', requiredRoles);
    // // Validate authorized roles for route
    // const { user } = context.switchToHttp().getRequest();
    // const isAuthorized = requiredRoles.some((role) =>
    //   user.roles?.includes(role),
    // );

    // if (isAuthorized) return true;
    return super.canActivate(context);
  }
}
