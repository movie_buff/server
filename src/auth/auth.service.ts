import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as argon from 'argon2';
import config from 'config';
import { User } from 'src/users/user.entity';
import { handleError } from 'src/utils/utils';
import { DataSource } from 'typeorm';
import { UsersService } from '../users/users.service';
import { AuthResponse } from './dto/auth.response';
import { LoginInput } from './dto/login.input';
import { SignUpInput } from './dto/sign-up.input';
import { TokenData } from './types';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private dataSource: DataSource,
    private jwtService: JwtService,
  ) {}

  async validateUser({ email, password }: LoginInput): Promise<User> {
    const user = await this.usersService.findOneBy(email);
    if (user && user.hashedPassword === password) {
      return user;
    }
    return null;
  }

  async signUp(signUpInput: SignUpInput): Promise<AuthResponse> {
    const hashedPassword = await argon.hash(signUpInput.password);
    try {
      const user = await this.dataSource.transaction(async (manager) => {
        return await manager.save(User, {
          ...signUpInput,
          hashedPassword,
        });
      });

      const { accessToken, refreshToken } = this.createTokens(
        user.userId,
        user.email,
      );

      await this.updateRefreshToken(user.userId, refreshToken);

      return { accessToken, refreshToken, user };
    } catch (error) {
      handleError(error);
    }
  }

  async login({ email }: LoginInput): Promise<User> {
    try {
      const user = this.dataSource.transaction(async (manager) => {
        return await manager.findOneBy(User, { email });
      });

      return user;
    } catch (error) {
      handleError(error);
    }
  }

  createTokens(userId: string, email: string): TokenData {
    const accessToken = this.jwtService.sign(
      {
        userId,
        email,
      },
      {
        expiresIn: '10s',
        secret: config.get<string>('jwtPrivateKey'),
      },
    );

    const refreshToken = this.jwtService.sign(
      {
        userId,
        email,
        accessToken,
      },
      {
        expiresIn: '7d',
        secret: config.get<string>('jwtRefreshTokenPrivateKey'),
      },
    );

    return { accessToken, refreshToken };
  }

  async updateRefreshToken(userId: string, refreshToken: string) {
    const hashedRefreshToken = await argon.hash(refreshToken);
    this.dataSource.transaction(async (manager) => {
      await manager.update(User, { userId }, { hashedRefreshToken });
    });
  }
}
