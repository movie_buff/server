import { Injectable } from '@nestjs/common';
import axios from 'axios';
import config from 'config';
import { handleError } from 'src/utils/utils';
import { MoviesSizeArgs } from './dto/args/movies.args';
import { FavMovieInput } from './dto/inputs/fav-movie.input';
import { Movie } from './movie.entity';

@Injectable()
export class MoviesService {
  private apiKey = config.get<string>('apiKey');

  async create(data: FavMovieInput): Promise<Movie> {
    return {} as any;
  }

  async findMovies(search: string): Promise<Movie | Movie[] | string> {
    try {
      const { data, status } = await axios<{ Search: Movie[] }>({
        method: 'GET',
        timeout: 5000, // 5 seconds timeout
        url: `http://www.omdbapi.com/?s=${search}&apikey=${this.apiKey}`,
      });

      // console.log(JSON.stringify(data, null, 4));
      // 👇️ "response status is: 200"
      console.log('response status is: ', status);

      if (data instanceof Movie) {
        return data;
      }
      return data.Search;
    } catch (error: any) {
      handleError(error);
    }
  }

  async findOneById(imdbID: string): Promise<Movie> {
    try {
      const { data, status } = await axios<Movie>({
        method: 'GET',
        timeout: 5000, // 5 seconds timeout
        url: `http://www.omdbapi.com/?i=${imdbID}&apikey=${this.apiKey}`,
      });

      // console.log(JSON.stringify(data, null, 4));
      // 👇️ "response status is: 200"
      console.log('response status is: ', status);

      return data;
    } catch (error: any) {
      handleError(error);
    }
  }

  async findAll(moviesArgs: MoviesSizeArgs): Promise<Movie[]> {
    return {} as any;

    // To be used later
    // axios.all([
    //   axios.get('https://api.github.com/users/iliakan'),
    //   axios.get('https://api.github.com/users/taylorotwell')
    // ])
    // .then(axios.spread((obj1, obj2) => {
    //   // Both requests are now complete
    //   console.log(obj1.data.login + ' has ' + obj1.data.public_repos + ' public repos on GitHub');
    //   console.log(obj2.data.login + ' has ' + obj2.data.public_repos + ' public repos on GitHub');
    // }));
  }

  async remove(id: string): Promise<boolean> {
    return true;
  }
}
