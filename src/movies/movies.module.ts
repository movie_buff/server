import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MoviesResolver } from './graphql/movies.resolver';
import { MoviesSubscriber } from './graphql/movies.subscriber';
import { Movie } from './movie.entity';
import { MoviesService } from './movies.service';

@Module({
  imports: [TypeOrmModule.forFeature([Movie])],
  providers: [MoviesResolver, MoviesService, MoviesSubscriber],
})
export class MoviesModule {}
