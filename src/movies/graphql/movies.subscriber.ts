import {
  DataSource,
  EntitySubscriberInterface,
  EventSubscriber,
  InsertEvent,
} from 'typeorm';
import { Movie } from '../movie.entity';

@EventSubscriber()
export class MoviesSubscriber implements EntitySubscriberInterface<Movie> {
  constructor(dataSource: DataSource) {
    dataSource.subscribers.push(this);
  }

  listenTo() {
    return Movie;
  }

  beforeInsert(event: InsertEvent<Movie>) {
    console.log(`BEFORE Favorite Movie INSERTED: `, event.entity);
  }
}
