import { NotFoundException } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { Role } from 'src/auth/roles/role.enum';
import { FavMovieInput } from '../dto/inputs/fav-movie.input';
import { Movie } from '../movie.entity';
import { MoviesService } from '../movies.service';

@Resolver()
export class MoviesResolver {
  constructor(private readonly moviesService: MoviesService) {}

  @Roles([Role.Admin, Role.User])
  @Query(() => [Movie])
  async moviesBySearch(
    @Args('search') search: string,
  ): Promise<Movie | Movie[] | string> {
    const data = await this.moviesService.findMovies(search);
    console.log(data);
    if (typeof data === 'string') {
      throw new NotFoundException(search);
    }

    return data;
  }

  @Query(() => Movie)
  async movieById(@Args('imdbID') imdbID: string): Promise<Movie | string> {
    const data = await this.moviesService.findOneById(imdbID);
    if (typeof data === 'string') {
      throw new NotFoundException(imdbID);
    }

    return data;
  }

  @Mutation(() => Movie)
  async addFavMovie(
    @Args('favMovieData') favMovieData: FavMovieInput,
  ): Promise<Movie> {
    const favMovie = await this.moviesService.create(favMovieData);
    return favMovie;
  }

  @Mutation(() => Boolean)
  async removeMovie(@Args('id') id: string) {
    return this.moviesService.remove(id);
  }
}
