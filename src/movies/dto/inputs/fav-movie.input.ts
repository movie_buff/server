import { Field, InputType } from '@nestjs/graphql';
import { Length } from 'class-validator';

@InputType()
export class FavMovieInput {
  @Field()
  @Length(30, 255)
  imdbid: string;
}
