import { Field, ObjectType } from '@nestjs/graphql';
import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';

enum MovieType {
  Movie = 'Movie',
  Series = 'Series',
}

@Entity()
@ObjectType({ description: 'movie' })
export class Movie {
  @PrimaryColumn()
  @Field(() => String)
  imdbID: string;

  @Column()
  @Field(() => String)
  Title: string;

  @Column()
  @Field(() => String)
  Year: string;

  @Column()
  @Field(() => String)
  Type: MovieType;

  @Column()
  @Field(() => String)
  Poster: string;

  @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  @Field()
  createDateTime: Date;

  @UpdateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  @Field()
  lastChangedDateTime: Date;
}
