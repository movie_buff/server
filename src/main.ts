// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();
import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import config from 'config';
import { AppModule } from './app.module';
import redisClient from './utils/connectRedis';
import validateEnv from './utils/validateEnv';

// Validate environment variables
validateEnv();

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: ['error', 'warn'],
  });
  app.useGlobalPipes(new ValidationPipe());
  const message = await redisClient.get('try');
  console.log(message);
  const port = config.get<number>('port');
  await app.listen(port);
}
bootstrap();
