## Description

**Movie Buff Server was built using:**

- NestJs (NodeJs TypeScript framework)
- TypeORM
- Docker (Postgres & Redis)

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod

