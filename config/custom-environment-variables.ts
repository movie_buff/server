export default {
  port: 'PORT',
  apiKey: 'OMDB_API_KEY',
  postgresConfig: {
    host: 'POSTGRES_HOST',
    port: 'POSTGRES_PORT',
    username: 'POSTGRES_USER',
    password: 'POSTGRES_PASSWORD',
    database: 'POSTGRES_DB',
  },
  jwtPrivateKey: 'JWT_ACCESS_TOKEN_PRIVATE_KEY',
  jwtPublicKey: 'JWT_ACCESS_TOKEN_PUBLIC_KEY',
  jwtRefreshTokenPrivateKey: 'JWT_REFRESH_TOKEN_PRIVATE_KEY',
  jwtRefreshTokenPublicKey: 'JWT_REFRESH_TOKEN_PUBLIC_KEY',
};
