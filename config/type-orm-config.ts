// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();
import config from 'config';
import 'reflect-metadata';
import { Movie } from 'src/movies/movie.entity';
import { User } from 'src/users/user.entity';

const postgresConfig = config.get<{
  host: string;
  port: number;
  username: string;
  password: string;
  database: string;
}>('postgresConfig');

export default {
  ...postgresConfig,
  synchronize: true,
  logging: false,
  entities: [User, Movie],
  migrations: ['src/database/migrations/**/*{.ts,.js}'],
  subscribers: ['src/database/subscribers/**/*{.ts,.js}'],
};
